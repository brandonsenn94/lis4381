# Mobile Web Application Development

## Brandon Senn 

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/brandonsenn94/lis4381/src/master/a1/README.md)

    * Install Ampps
    * Screenshot of Running JDK Java Hello
    * Screenshot of running Android Studio My First App
    * Git commands w/ short descriptions
    * Bitbucket Repo links
        * This assignment 
        * Completed tutorial repo [BitbucketStationLocations](https://bitbucket.org/brandonsenn94/bitbucketstationlocations/src)    

2. [A2 README.md](https://bitbucket.org/brandonsenn94/lis4381/src/master/a2/README.md)
    * Skillshare 2 and 3
    * Recipe App (First and second user interface)

3. [A3 README.md](https://bitbucket.org/brandonsenn94/lis4381/src/master/a3/README.md)
    * ERD Screenshot
    * Android First/Second Interface
    * Links to A3.sql and A3.mwb

4. [A4 README.md](https://bitbucket.org/brandonsenn94/lis4381/src/master/a4/README.md)
    * Screenshots of web application
    * Skillsets 10-12

5. [A5 README.md](https://bitbucket.org/brandonsenn94/lis4381/src/master/a5/README.md)
    * Server Side Validation Screenshots
    * Skillsets 13-15

6. [P1 README.md](https://bitbucket.org/brandonsenn94/lis4381/src/master/p2/README.md)
    * Android First/Second Interfaces
    * Skillset 7-9

7. [P2 README.md](https://bitbucket.org/brandonsenn94/lis4381/src/master/p2/README.md)
    * Basic CRUD (Create, Read, Update, Delete) functionality
    * RSS Feed Page