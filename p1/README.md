# LIS4381 - Mobile Web Application Development

## Brandon Senn 

### Project 1 Requirements:
- Android Screenshots (1st and 2nd user interfaces)
- Skillsets 7-9

--- 

|  First Interface  |  Second Interface  |
|  ---  |  ---  |
| ![First Interface](https://bitbucket.org/brandonsenn94/lis4381/raw/55893ab8acc4a25b7bf6a0098583c571708e5358/P1/AndroidScreenshots/First%20Interface.png) | ![Second Interface](https://bitbucket.org/brandonsenn94/lis4381/raw/55893ab8acc4a25b7bf6a0098583c571708e5358/P1/AndroidScreenshots/Second%20Interface.png) |

---

| Skillset 7 (Random Array) | Skillset 8 (Largest of Three Numbers) | Skillset 9 (Runtime Data Validation) |
|  ---  | --- | --- |
| ![Skillset 7](https://bitbucket.org/brandonsenn94/lis4381/raw/55893ab8acc4a25b7bf6a0098583c571708e5358/P1/SS/Res/RandomArray.png) | ![Skillset 8 (Largest 3 numbers)](https://bitbucket.org/brandonsenn94/lis4381/raw/55893ab8acc4a25b7bf6a0098583c571708e5358/P1/SS/Res/LargestThreeNumbers.png) | ![Skillset 8](https://bitbucket.org/brandonsenn94/lis4381/raw/55893ab8acc4a25b7bf6a0098583c571708e5358/P1/SS/Res/ArrayRuntimeValidation.png)


