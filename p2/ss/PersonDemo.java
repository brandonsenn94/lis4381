
public class PersonDemo{
    public static void main(String[] argv){
        Person a = new Person();
        Person b = new Person("Jane", "Smith", 35);
        System.out.println("////////////below are default constructor values///////////");
        System.out.println("");
        System.out.println("FName: " + a.getFname());
        System.out.println("Lname: " + a.getLName());
        System.out.println("Age: " + a.getAge());
        System.out.println("");
        System.out.println("////////////below are user-entered constructor values///////////");
        System.out.println("");
        System.out.println("FName: " + b.getFname());
        System.out.println("Lname: " + b.getLName());
        System.out.println("Age: " + b.getAge());
        System.out.println("");
        System.out.println("Inside person constructor with parameters.");
        System.out.println("");
        System.out.println("FName: " + b.getFname());
        System.out.println("Lname: " + b.getLName());
        System.out.println("Age: " + b.getAge());
        System.out.println("");
        System.out.println("////////////Below using setter values to pass literal values then print() method to display values.//////////");
        a.setFName("Bob");
        a.setLName("Wilson");
        a.setAge(42);
        System.out.println("Fname: " + a.getFname() + " Lname: " + a.getLName() + " Age: " + a.getAge());
    }
}



class Person{
    private String fname;
    private String lname;
    private int age;

    Person(){
        this.fname = "John";
        this.lname = "Doe";
        this.age = 21;
    }

    Person(String a, String b, int c){
        this.fname = a;
        this.lname = b;
        this.age = c;
    }

    public String getFname(){
        return fname;
    }

    public String getLName(){
        return lname;
    }

    public int getAge(){
        return age;
    }

    public void setFName(String a){
        this.fname = a;
    }

    public void setLName(String b){
        this.lname = b;
    }

    public void setAge(int c){
        this.age = c;
    }


}
