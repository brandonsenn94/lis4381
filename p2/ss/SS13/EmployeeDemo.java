package SS13;

public class EmployeeDemo{
    public static void main(String[] argv){
        Person a = new Person();
        Person b = new Person("Jane", "Smith", 35);
        System.out.println("////////////below are default constructor values///////////");
        System.out.println("");
        System.out.println("FName: " + a.getFname());
        System.out.println("Lname: " + a.getLName());
        System.out.println("Age: " + a.getAge());
        System.out.println("");
        System.out.println("////////////below are user-entered constructor values///////////");
        System.out.println("");
        System.out.println("FName: " + b.getFname());
        System.out.println("Lname: " + b.getLName());
        System.out.println("Age: " + b.getAge());
        System.out.println("");
        System.out.println("Inside person constructor with parameters.");
        System.out.println("");
        System.out.println("FName: " + b.getFname());
        System.out.println("Lname: " + b.getLName());
        System.out.println("Age: " + b.getAge());
        System.out.println("");
        System.out.println("////////////Below using setter values to pass literal values then print() method to display values.//////////");
        a.setFName("Bob");
        a.setLName("Wilson");
        a.setAge(42);
        System.out.println("Fname: " + a.getFname() + " Lname: " + a.getLName() + " Age: " + a.getAge());
        System.out.println("////////////Below are derived class user-entered values://////////");
        System.out.println("Inside person default constructor");
        System.out.println("");
        System.out.println("Inside Employee default constructor");
        System.out.println("");
        Employee d = new Employee();
        
    }
}