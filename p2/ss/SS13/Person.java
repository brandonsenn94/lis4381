package SS13;

public class Person{
    private String fname;
    private String lname;
    private int age;

    Person(){
        this.fname = "John";
        this.lname = "Doe";
        this.age = 21;
    }

    Person(String a, String b, int c){
        this.fname = a;
        this.lname = b;
        this.age = c;
    }

    public String getFname(){
        return fname;
    }

    public String getLName(){
        return lname;
    }

    public int getAge(){
        return age;
    }

    public void setFName(String a){
        this.fname = a;
    }

    public void setLName(String b){
        this.lname = b;
    }

    public void setAge(int c){
        this.age = c;
    }


}