# LIS4381 - Mobile Web Application Development

## Brandon Senn 

### Project 5 Requirements:
*	Basic CRUD for page created for A5
* 	RSS Feed

| Before Edit | Editing Form | Record Saved |
| --- | --- | --- |
| ![Before Edit](img/BeforeEdit.png) | ![Editing Form](img/BeforeSaving.png) | ![Record Saved](img/AfterEdit.png)|

| Failed Validation |
| --- |
| ![Failed Validation](img/failedvalidation.png) |

| Record Deleted |
| --- |
| ![Record Deleted](img/DeletedRecord.png) |

| RSS Feed |
| --- | 
| ![Calculator Before Adding](img/RSSFeed.png) | 