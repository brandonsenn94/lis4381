<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Brandon">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Developed ERD and also developed app that calculates total price of a kitchen supply store order.
				</p>

				<h4>My Kitchen Supply Store (First screenshot)</h4>
				<img src="img/KitchenSupplies_FirstInterface.png" class="img-responsive center-block" alt="First screenshot">

				<h4>My Recipe app (Second screenshot)</h4>
				<img src="img/KitchenSupplies_Calculated.png" class="img-responsive center-block" alt="Second screenshot">

				<h4>ERD</h4>
				<img src="img/A3_ERD.png" class ="img-responsive center-block" alt="ERD">

				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
