# LIS4381 - Mobile Web Application Development

## Brandon Senn 

### Assignment 3 Requirements:
- ERD Screenshot
- Android First/Second Interface
- Links to A3.sql and A3.mwb

| ERD  |
|   ---   |
|![ERD](https://bitbucket.org/brandonsenn94/lis4381/raw/2d11b6567f1582259217f48353651ae14aaa045e/A3/Res/A3_ERD.png)| 


--- 

|  First Interface  |  Second Interface  |
|  ---  |  ---  |
| ![First Interface](https://bitbucket.org/brandonsenn94/lis4381/raw/2d11b6567f1582259217f48353651ae14aaa045e/A3/Res/KitchenSupplies_FirstInterface.png) | ![Second Interface](https://bitbucket.org/brandonsenn94/lis4381/raw/2d11b6567f1582259217f48353651ae14aaa045e/A3/Res/KitchenSupplies_Calculated.png) |

---

### Links:
- [A3.sql](https://bitbucket.org/brandonsenn94/lis4381/src/master/A3/Docs/A3.sql)
- [A3.mwb](https://bitbucket.org/brandonsenn94/lis4381/src/master/A3/Docs/A3%20DB.mwb)