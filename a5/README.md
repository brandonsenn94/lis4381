# LIS4381 - Mobile Web Application Development

## Brandon Senn 

### Assignment 5 Requirements:
*	Develop server-side validation for petstore form in A4.
* 	Use A4 form to submit records to Petstore DB.
*	Skillset 14-15

| Petstore Form | Record Added | Failed Validation |
| --- | --- | --- |
| ![Before saving](img/BeforeSaving.png) | ![Save Successful](img/SaveSuccessful.png) | ![Failed Validation](img/error.png) |

| Addition Example | Result |
| --- | --- |
| ![Calculator Before Adding](img/calculator_beforeresult.png) | ![Calculator After adding](img/calculator_afterresult.png) |

| Division Example | Result | 
| --- | --- |
| ![Before](img/calculator_beforeresult_division.png) | ![After](img/calculator_afterresult_division.png)

| Writing to File | Result |
| --- | --- |
| ![Before](img/write_read_home.png) | ![After](img/write_read_afterEnter.png)
