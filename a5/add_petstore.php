<!DOCTYPE html>
<html lang="en">
<head>



<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Assignment 5 displays basic CRUD application..">
	<meta name="author" content="Brandon Senn">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment5</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
						<p class ="text-left">
						<strong> Description </strong>
						<ul>
						<li class="text-left"> Basic server-side validation.</li>
						<li class="text-left"> Displays user entered data and allows users to add data. </li>
						</p>
					</div>

					<h2>Pet Stores</h2>

					<?php            ?>
						<form id="add_petstore" method="post" class="form-horizontal" action="add_petstore_process.php">

								<div class="form-group">
										<label class="col-sm-4 control-label">Name:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="30" name="name" placeholder ="(max 30 char)" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Street:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="30" name="street" placeholder ="(max 30 char)" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">City:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="30" name="city" placeholder ="(max 30 char)" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">State:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="2" name="state" placeholder ="Example: FL" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Zip Code:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="9" name="zipCode" placeholder="(5 or 9 digits, no dashes." />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Phone:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="10" name="phone" placeholder="(10 digits no other characters.)" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Email:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="100" name="email" placeholder="Example: jdoe@aol.com" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">URL:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="100" name="url" placeholder="Example: www.google.com" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">YTD Sales:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="11" name="ytdsales" placeholder="Example: 100.00 (no other characters.)" />
										</div>
								</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Notes:</label>
										<div class="col-sm-4">
												<input type="text" class="form-control" maxlength="255" name="Notes" />
										</div>
								</div>

								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" name="add" value="Add">Add</button>
										</div>
								</div>
						</form>

			<?php include_once "global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
	<?php //include_once("../js/include_js.php"); ?> 

<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
 $(document).ready(function() {

	$('#add_petstore').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					name: {
							validators: {
									notEmpty: {
									 message: 'Name required'
									},
									stringLength: {
											min: 1,
											max: 30,
									 message: 'Name no more than 30 characters'
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[\w\-\s]+$/,
										message: 'Name can only contain letters, numbers, hyphens, and underscore'
									},									
							},
					},

					street: {
							validators: {
									notEmpty: {
											message: 'Street required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'Street no more than 30 characters'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[a-zA-Z0-9,\-\s\.]+$/,		
									message: 'Street can only contain letters, numbers, commas, hyphens, or periods'
									},									
							},
					},

					city: {
							validators: {
									notEmpty: {
											message: 'City required'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'City no more than 30 characters'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[a-zA-Z0-9\-\s]/,		
									message: 'City can only contain letters, numbers, hyphens and space characters'
									},									
							},
					},

					state: {
							validators: {
									notEmpty: {
											message: 'State required'
									},
									stringLength: {
											min: 2,
											max: 2,
											message: 'State no more than 2 characters'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[a-zA-Z]+$/,		
									message: 'State can only contain letters'
									},									
							},
					},

					zipCode: {
							validators: {
									notEmpty: {
											message: 'Zip Code required'
									},
									stringLength: {
											min: 5,
											max: 9,
											message: 'Zipcode no more than 9 characters'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[0-9]+$/,		
									message: 'Zip Code may only contain numbers.'
									},									
							},
					},

					phone: {
							validators: {
									notEmpty: {
											message: 'Phone number required'
									},
									stringLength: {
											min: 10,
											max: 10,
											message: 'Phone number must contain 10 numbers.'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[0-9]+$/,		
									message: 'Phone numbers may only contain numbers.'
									},									
							},
					},

					email: {
							validators: {
									notEmpty: {
											message: 'Email required'
									},
									stringLength: {
											min: 1,
											max: 100,
											message: 'Email no more than 100 characters'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/,		
									message: 'Must contain valid email.'
									},									
							},
					},

					url: {
							validators: {
									notEmpty: {
											message: 'URL required'
									},
									stringLength: {
											min: 1,
											max: 100,
											message: 'URL must not be longer than 100 characters.'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/,		
									message: 'URL must only contain letters, numbers, hyphens, and periods.'
									},									
							},
					},

					ytdsales: {
							validators: {
									notEmpty: {
											message: 'YTD Sales required'
									},
									stringLength: {
											min: 1,
											max: 11,
											message: 'Must contain less than 11 digits total.'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[0-9\.]+$/,		
									message: 'Must only contain digits and periods.'
									},									
							},
					},
			}
	});
});
</script>

</body>
</html>
