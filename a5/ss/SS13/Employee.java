package SS13;

class Employee extends Person{
    private int ssn;
    private char gender;

    Employee(){
        super();
        this.ssn=000000000;
        this.gender='f';
    }

    Employee(int a, char b){
        super();
        this.ssn=a;
        this.gender=b;
    }

    public int getSSN(){
        return this.ssn;
    }

    public char getGender(){
        return this.gender;
    }

    public void setSSN(int a){
        this.ssn=a;
    }

    public void setGender(char b){
        this.gender=b;
    }
    
}
