# LIS4381 - Mobile Web Application Development

## Brandon Senn 

### Assignment 2 Requirements:

* First/Second user interface (Android app)
* Skillsets 2 and 3 screenshots (LargestofTwoIntegers.java and ArraysAndLoops.java)

| First User Interface | Second User Interface |
| ------ | ------ |
| ![First User Interface](https://bitbucket.org/brandonsenn94/lis4381/raw/9a71636f3e2c1af393bfb5f2a7a6abb36701ee22/A2/Android%20Screenshots/First_screen.png) | ![Second User Interface](https://bitbucket.org/brandonsenn94/lis4381/raw/9a71636f3e2c1af393bfb5f2a7a6abb36701ee22/A2/Android%20Screenshots/second_screen.png) |

| Skillshare 2 (LargestofTwoIntegers.java) | Skillshare 3 (ArraysandLoops.java) |
| ---- | ---- |
| ![Skillshare 2](https://bitbucket.org/brandonsenn94/lis4381/raw/9a71636f3e2c1af393bfb5f2a7a6abb36701ee22/A2/Java%20Screenshots/LargestNumber.png) | ![Skillshare 3](https://bitbucket.org/brandonsenn94/lis4381/raw/9a71636f3e2c1af393bfb5f2a7a6abb36701ee22/A2/Java%20Screenshots/ArraysAndLoops.png) |



